#ifndef GEOSP_GEOMGROUP_H
#define GEOSP_GEOMGROUP_H

#include <QJsonValue>
#include <QJsonObject>
#include <QSharedPointer>
#include <QVariant>

#include "GeoSp_Point.h"

class GeoSp_GeomGroup
{
public:
	GeoSp_GeomGroup(){}
	~GeoSp_GeomGroup(){};

	QSharedPointer<GeoSp_Point> m_point;

	bool operator = (const QJsonValue& other)
	{
		if (other.isObject())
		{
			QJsonObject no = other.toObject();
			if (no["type"].toString() == "Point")
			{
				m_point = QSharedPointer<GeoSp_Point>::create();
				if (no["coordinates"].isArray())
				{
					QJsonArray cr = no["coordinates"].toArray();
					for (auto x : cr)
					{
						m_point->m_coordinates.push_back(x.toDouble());
					}
				}
				return true;
			}
		}
		else
		{
			m_point.clear();
		}
		return false;
	}

	operator QJsonValue () const
	{
		qDebug () << "GeoSp_GeomGroup operator QJsonValue";
		if (!m_point)
		{
			return QJsonValue::Null;
		}
		QJsonObject ro;
		ro.insert("type", "Point");
		ro.insert("coordinates", qx::cvt::detail::QxConvert_ToJson<QVector<double>>::toJson(m_point->m_coordinates, QString()));
		return ro;
	}

	bool operator = (const QString& other)
	{
		QJsonDocument od = QJsonDocument::fromJson(other.toUtf8());
		if (od.isObject())
		{
			QJsonObject no = od.object();
			if (no["type"].toString() == "Point")
			{
				m_point = QSharedPointer<GeoSp_Point>::create();
				if (no["coordinates"].isArray())
				{
					QJsonArray cr = no["coordinates"].toArray();
					for (auto x : cr)
					{
						m_point->m_coordinates.push_back(x.toDouble());
					}
				}
				return true;
			}
		}
		else
		{
			m_point.clear();
		}
		return false;
	}

	operator QString () const
	{
		qDebug () << "GeoSp_GeomGroup operator QString";
		if (!m_point)
		{
			return QStringLiteral("");
		}
		QJsonObject ro;
		ro.insert("type", "Point");
		ro.insert("coordinate", qx::cvt::detail::QxConvert_ToJson<QVector<double>>::toJson(m_point->m_coordinates, QString()));
		return QString::fromUtf8(QJsonDocument(ro).toJson());
	}

	bool operator = (const QVariant & other)
	{
		switch (static_cast<QMetaType::Type>(other.type()))
		{
		case QMetaType::QJsonObject:
		{
			QJsonObject no = other.toJsonObject();
			if (no["type"].toString() == "Point")
			{
				m_point = QSharedPointer<GeoSp_Point>::create();
				if (no["coordinates"].isArray())
				{
					QJsonArray cr = no["coordinates"].toArray();
					for (auto x : cr)
					{
						m_point->m_coordinates.push_back(x.toDouble());
					}
				}
				return true;
			}
			return false;
		}
		case QMetaType::QJsonValue:
		{
			return *this = other.toJsonValue();
		}
		case QMetaType::QJsonDocument:
		{
			QJsonDocument dc = other.toJsonDocument();
			if (dc.isObject())
			{
				return *this = QJsonValue (dc.object());
			}
			return false;
		}
		case QMetaType::QString:
		{
			return *this = other.toString();
		}
		default:
		m_point.clear();
		return false;
		}
	}

	operator QVariant () const
	{
		qDebug () << "GeoSp_GeomGroup operator QVariant";
		return QVariant(QJsonValue (*this));
	}
};

namespace qx {
namespace cvt {
namespace detail {

template <> struct QxConvert_ToString< GeoSp_GeomGroup >
{
	static inline QString toString(const GeoSp_GeomGroup & t, const QString & format, int index, qx::cvt::context::ctx_type ctx)
	{
		Q_UNUSED(format); Q_UNUSED(index); Q_UNUSED(ctx); return t;
	}
};
template <> struct QxConvert_FromString< GeoSp_GeomGroup >
{
	static inline qx_bool fromString(const QString & s, GeoSp_GeomGroup & t, const QString & format, int index, qx::cvt::context::ctx_type ctx)
	{
		Q_UNUSED(format); Q_UNUSED(index); Q_UNUSED(ctx); return t = s;
	}
};

template <> struct QxConvert_ToJson< GeoSp_GeomGroup >
{
	static inline QJsonValue toJson(const GeoSp_GeomGroup & t, const QString & format)
	{
		Q_UNUSED(format); return t;
	}
};
template <> struct QxConvert_FromJson< GeoSp_GeomGroup >
{
	static inline qx_bool fromJson(const QJsonValue & s, GeoSp_GeomGroup & t, const QString & format)
	{
		Q_UNUSED(format); return t = s;
	}
};

template <> struct QxConvert_ToVariant< GeoSp_GeomGroup >
{
	static inline QVariant toVariant(const GeoSp_GeomGroup & t, const QString & format, int index, qx::cvt::context::ctx_type ctx)
	{
		Q_UNUSED(format); Q_UNUSED(index); Q_UNUSED(ctx); return t;
	}
};
template <> struct QxConvert_FromVariant< GeoSp_GeomGroup >
{
	static inline qx_bool fromVariant(const QVariant & s, GeoSp_GeomGroup & t, const QString & format, int index, qx::cvt::context::ctx_type ctx)
	{
		Q_UNUSED(format); Q_UNUSED(index); Q_UNUSED(ctx); return t = s;
	}
};
}
}
}

#endif // GEOSP_GEOMGROUP_H
