#ifndef CITIZEN_H
#define CITIZEN_H

#include "common.h"
#include "location.h"
#include <QString>

class TEST_DLL_EXP citizen
{
public:

	QString id{};
	QString name{};

	QString locationOid;
	ptrLocation locationObj;

	citizen (){}
	citizen(ptrLocation &ploc) : locationObj(ploc) {qDebug () << "pLoc constructed in cit";}
	virtual ~citizen(){}
};

//typedef QSharedPointer<citizen> ptrCitizen;
TEST_PRIMARY_KEY_QSTRING(citizen)
TEST_HPP_NOBASE_REG(citizen)

#endif // CITIZEN_H
