#ifndef LOCATION_H
#define LOCATION_H

#include "common.h"
#include <QString>
#include <QSharedPointer>
#include "GeoSp_GeomGroup.h"

class citizen;
typedef QSharedPointer<citizen> ptrCitizen;

class TEST_DLL_EXP StuffInLocation
{
public:
	QString m_someStuff{};

	StuffInLocation(){}
	virtual ~StuffInLocation(){}
};

TEST_HPP_NOBASE_REG(StuffInLocation)

class TEST_DLL_EXP location : public StuffInLocation
{
public:

	QString id{};
	int x{};
	int y{};

	QString citizenId;
	ptrCitizen citizenObj;
	QSharedPointer<GeoSp_GeomGroup> m_geoObj;

	location(){}
//	location (ptrCitizen &pcit) : citizenObj(pcit) {}
	virtual ~location(){}
};

typedef QSharedPointer<location> ptrLocation;

TEST_PRIMARY_KEY_QSTRING(location)
TEST_HPP_REG(location, StuffInLocation, 0)

#endif // LOCATION_H
