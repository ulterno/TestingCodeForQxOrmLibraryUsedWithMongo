#include "citizen.h"

TEST_CPP_REG(citizen)

namespace qx
{
	template<> void register_class (QxClass<citizen> &t)
	{
		t.id (&citizen::id, "_id");
		t.data(&citizen::name, "name");

		auto idField = t.data(&citizen::locationOid, "location");
		auto rel = t.relationOneToOne(&citizen::locationObj, "locationObj");
		rel->linkRelationKeyTo(idField);
	}
}
