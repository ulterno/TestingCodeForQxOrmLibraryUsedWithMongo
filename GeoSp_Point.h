#ifndef GEOSP_POINT_H
#define GEOSP_POINT_H

#include "common.h"

class GeoSp_Point
{
public:

	QString m_type = "Point";
	QVector<double> m_coordinates;

	GeoSp_Point(){}
	virtual ~GeoSp_Point(){}
};

TEST_HPP_NOBASE_REG (GeoSp_Point)

#endif // GEOSP_POINT_H
