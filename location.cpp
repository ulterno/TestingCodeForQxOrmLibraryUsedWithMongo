#include "location.h"
#include "citizen.h"

TEST_CPP_REG(StuffInLocation)

namespace qx
{
	template<> void register_class(QxClass<StuffInLocation>&t)
	{
		t.data(&StuffInLocation::m_someStuff, "someStuff");
	}
}

TEST_CPP_REG(location)

namespace qx
{
	template<> void register_class(QxClass<location> & t)
	{
		t.id(&location::id, "_id");
		t.data(&location::x, "x");
		t.data(&location::y, "y");

		auto idField = t.data(&location::citizenId, "citizen");
		auto rel = t.relationOneToOne(&location::citizenObj, "citizenObj");
		rel->linkRelationKeyTo(idField);

		t.data(&location::m_geoObj, "geoObj");
	}
}
