#include "GeoSp_Point.h"

TEST_CPP_REG (GeoSp_Point)

namespace qx
{
	template<> void register_class(QxClass<GeoSp_Point>&t)
	{
		t.data(&GeoSp_Point::m_type, "type");
		t.data(&GeoSp_Point::m_coordinates, "coordinates");
	}
}
