#include <QCoreApplication>

#include "citizen.h"

#include <QxCollection/QxCollection.h>
#include <QxDataMember/IxDataMember.h>
#include <QxDataMember/IxDataMemberX.h>
#include <QxDataMember/QxDataMember.h>
#include <QxDataMember/QxDataMemberX.h>
#include <QxRegister/QxClass.h>
#include <QxRegister/QxClassX.h>
#include <QxRegister/IxClass.h>

#include <bson.h>

enum relation_type { no_relation, one_to_one, one_to_many, many_to_one, many_to_many };

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	int bytes_in_oid = 12;
	bson_oid_t newOid;
	bson_oid_init(&newOid, nullptr);
	QByteArray ba;
	for (int iter = 0; iter < bytes_in_oid; iter++)
	{
		ba.append(newOid.bytes[iter]);
	}
	qDebug () << "New ObjectID: " << ba.toHex();

	qx::QxClassX::registerAllClasses();

	/*qDebug () << */qx::QxClassX::getClass("location")->dumpClass();/* <<*/ qx::QxClassX::getClass("citizen")->dumpClass();
			qDebug () << "\n___________________________";
//	/*qDebug () << */qx::QxClassX::dumpSqlSchema();
	qDebug () << "_______________________________________________";
	qx::QxClassX::dumpAllClasses();
	qDebug () << "_______________________________________________";

	auto currentClass = qx::QxClassX::getClass("location");
	auto baseClass = currentClass->getBaseClass();
	auto dataMemberList = currentClass->getDataMemberX();
	auto bcDataMemberList = baseClass->getDataMemberX();
	long numberOfKeys = dataMemberList->count();
	qDebug () << "location has " << numberOfKeys << " keys:";
	for (int iter = 0; iter < numberOfKeys; iter++)
	{
		auto dm = dataMemberList->get(iter);
		QString relVal ("no_relation");
		if (dm->getSqlRelation())
		{
			switch (static_cast<relation_type>(dm->getSqlRelation()->getRelationType()))
			{
			case no_relation:
				relVal = "no_relation";
				break;
			case one_to_one:
				relVal = "one_to_one";
				break;
			case one_to_many:
				relVal = "one_to_many";
				break;
			case many_to_one:
				relVal = "many_to_one";
				break;
			case many_to_many:
				relVal = "many_to_many";
				break;
			}
		}
		qDebug () << dm->getKey() << dm->getName() << dm->getType() << dm->getSqlName() << "Relation: " << relVal;
	}
	long bcNumberOfKeys = bcDataMemberList->count();
	qDebug () << "With base class having " << bcNumberOfKeys;
	for (int iter = 0; iter < bcNumberOfKeys; iter++)
	{
		auto dm = bcDataMemberList->get(iter);
		QString relVal ("no_relation");
		if (dm->getSqlRelation())
		{
			switch (static_cast<relation_type>(dm->getSqlRelation()->getRelationType()))
			{
			case no_relation:
				relVal = "no_relation";
				break;
			case one_to_one:
				relVal = "one_to_one";
				break;
			case one_to_many:
				relVal = "one_to_many";
				break;
			case many_to_one:
				relVal = "many_to_one";
				break;
			case many_to_many:
				relVal = "many_to_many";
				break;
			}
		}
		qDebug () << dm->getKey() << dm->getName() << dm->getType() << dm->getSqlName() << "Relation: " << relVal;
	}

	qx::QxSqlDatabase *ndb;
	ndb = qx::QxSqlDatabase::getSingleton();
	ndb->setDriverName("QXMONGODB", true);
	ndb->setHostName("localhost", true);
	ndb->setPort(27017, true);
	ndb->setDatabaseName("another_testing_db_2", true);
	ndb->setConnectOptions("serverSelectionTimeoutMS=1000&connectTimeoutMS=300", true);

	QSqlError err;


	{
	qx_query aRy ("{\"dropDatabase\" : 1}");
	err = qx::dao::call_query(aRy);
	qDebug () << err;

	ptrLocation nl1 (new location{});
	nl1->x = 10;
	nl1->y = 20;
	////////////////////////////
	nl1->m_geoObj = QSharedPointer<GeoSp_GeomGroup>::create();
	QVector<double> coord;
	coord.push_back(13.42);
	coord.push_back(14.61);
	nl1->m_geoObj->m_point = QSharedPointer<GeoSp_Point>::create();
	nl1->m_geoObj->m_point->m_coordinates = coord;
	/// ///////////////////////
	citizen nc1(nl1);
	nc1.name = "sprite1";
	err = qx::dao::insert_with_all_relation(nc1);
	qDebug () << err;
	qx::dump(nc1);
	nc1.locationOid = nc1.locationObj->id;
	nc1.locationObj->citizenId = nc1.id;
	err = qx::dao::update(nc1); //	qx::dao::update_with_all_relation(nc1) causes one extra call to mongodb, so preferred to manually add the other one.
	qDebug () << err;
	nl1 = nc1.locationObj;

	err = qx::dao::update(nl1);
	qDebug () << err;
	qx::dump(nc1);
	}

qDebug () << "________________________Insertion Complete__________________________________";

/*	nameswapping: {
	auto citMember_lObj = qx::QxClassX::getDataMember("citizen", "locationObj");
	auto citMember_lId = qx::QxClassX::getDataMember("citizen", "location");

	citMember_lId->setName("temp123");
	citMember_lObj->setName("location");
	}
*/

	QList<citizen> nc;
	err = qx::dao::fetch_all_with_all_relation(nc);
	qDebug () << err;
	qx::dump(nc);

	qDebug () << "Location item in citizen";
	if (!nc.isEmpty())
	{
		qDebug () << nc.at(0).locationObj->id << nc.at(0).locationObj->x << nc.at(0).locationObj->y;
	}

	QList<location> nl;
	err = qx::dao::fetch_all_with_all_relation(nl);
	qDebug () << err;
	qx::dump(nl);

	qDebug () << "Citizen item in location";
	if (!nl.isEmpty())
	{
		qDebug () << nl.at(0).citizenObj->id << nl.at(0).citizenObj->name;
	}

	exit(0);
	return a.exec();
}
